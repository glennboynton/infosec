Organizationally Unique Identifiers

https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries

Company ID:  http://standards-oui.ieee.org/cid/cid.csv

Manufacturer ID:  http://standards-oui.ieee.org/manid/manid.csv

MAC Addresses (Large):  http://standards-oui.ieee.org/oui/oui.csv
